# Nmap Cheat Sheet

## Nmap Targeting 

### Scan a single IP 
	
```nmap 192.168.1.1```

### Scan a hostname 
	
```nmap www.domain.com```

### Scan an IP range 
	
```nmap 192.168.1.1-100``` 

### Scan a subnet 
	
```nmap 192.168.1.0/24```

### Scan from a predefined list 
	
```nmap -iL list.txt``` 

## Ports 

### Scan a single port 	

```nmap -p 22 192.168.1.1``` 

### Scan a range of ports 	

```nmap -p 1-20 192.168.1.1``` 

### Scan multiple ports 

```nmap -p 22,80,443 192.168.1.1``` 

### Scan Mixed TCP/UDP ports 
	
```nmap -p U:53,T:22 192.168.1.1``` 

### Scan 100 common ports 

```nmap -F 192.168.1.1``` 

### Scan top # ports 	

```nmap –top-ports 300 192.168.1.1``` 

### Scan ports linearly 
	
```nmap -r -p 1-1000 192.168.1.1``` 

### Scan all ports 
	
```nmap -p- 192.168.1.1``` 

## Scan types 

### TCP Connect Scan 

```nmap -sT 192.168.1.1``` 

### TCP SYN scan (Silent scan) 
	
```nmap -sS 192.168.1.1``` 

### UDP scan 

```nmap -sU -p 137,139 192.168.1.1``` 

### No ping scan 	

```nmap -Pn 192.168.1.1``` 

### Host Discovery (no ports) 

```nmap -sn 192.168.1.1``` 

### Version Scan 	

```nmap -sV 192.168.1.1``` 

### OS Detection 

```nmap -o 192.168.1.1``` 

## OS and Service Discovery 

### OS and Services 

```nmap -A 192.168.1.1``` 

### Standard service discovery 

```nmap -sV 192.168.1.1``` 

### Aggressive service discovery 

```nmap -sV –version-intensity 5 192.168.1.1``` 

### Light banner grabbing 

```nmap -sV –version-intensity 0 192.168.1.1``` 

## Aggregate Timing 

### Paranoid: Very slow 

```nmap -t0 192.168.1.1``` 

### Sneaky: Quite slow 	

```nmap -t1 192.168.1.1``` 

### Polite: Slows down 

```nmap -t2 192.168.1.1``` 

### Normal: Default 	

```nmap -t3 192.168.1.1``` 

### Aggressive: Fast and reliable 

```nmap -t4 192.168.1.1``` 

### Insane: Very aggressive 

```nmap -t5 192.168.1.1``` 

## Output Formats 

### Standard Nmap output 

```nmap -oN output.txt 192.168.1.1``` 

### XML format 

```nmap -oX output.txt 192.168.1.1``` 

### Greppable format 

```nmap -oG output.txt 192.168.1.1``` 

### All formats output 

```nmap -oA output.txt 192.168.1.1``` 

## NSE Scripts 

### Default scripts 

```nmap -sV -sC 192.168.1.1```

### Script help 

```nmap –script-help=ssl-heartbleed```

### NSE script scan 

```nmap -sV –script=ssl-heartbleed -p 443 192.168.1.1```

### Scan with scripts sets 

```nmap -sV –script=smb* 192.168.1.1```
